// ==UserScript==
// @name         Onboarding Extended
// @namespace    http://tampermonkey.net/
// @version      0.8
// @description  Onboaring Extended
// @author       Daniel Szlaski
// @resource     bootStrapTable https://rawgit.com/wenzhixin/bootstrap-table/master/dist/bootstrap-table.min.css
// @resource     bootStrapCSS https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css
// @resource     queryBuilderCSS https://cdn.jsdelivr.net/npm/jQuery-QueryBuilder/dist/css/query-builder.default.min.css
// @include      /^https\:\/\/apps\.mypurecloud\.[a-z]{2,3}.?[a-u]{0,2}\/onboarding/
// @require      https://code.jquery.com/jquery-3.4.1.slim.min.js
// @grant        GM_xmlhttpRequest
// @grant        GM_getValue
// @grant        GM_setValue
// @grant        GM_addStyle
// @grant        GM_getResourceText
// @grant        GM_getResourceURL
// @run-at       document-end
// ==/UserScript==

var $ = window.jQuery = window.jQuery.noConflict(true);

// Get URL
var addr = document.URL.split('//')[1].split('/')[0];
console.log('API url: ' + addr);

function GM_addScript (aScriptName) {

    var scriptElement = document.createElement( "script" );
    scriptElement.type = "text/javascript";
    scriptElement.src = aScriptName;
    document.body.appendChild(scriptElement);
}


// Add custom CSS to page
GM_addStyle (GM_getResourceText ("bootStrapCSS"));
GM_addStyle (GM_getResourceText ("bootStrapTable"));
GM_addStyle (GM_getResourceText ("queryBuilderCSS"));

// Add inline scritps
GM_addStyle(".customCell { word-break: break-all; overflow-x: scroll;margin-right: -30px; overflow:hidden; }");
GM_addStyle(".bootstrap-table .fixed-table-container .table thead th .th-inner { overflow: visible !important; white-space: pre-wrap !important; padding: 0.2rem !important; word-break: break-all }");
GM_addStyle(".bootstrap-table .fixed-table-container .table thead th { vertical-align: top !important} ");
GM_addStyle("#billing-organizations table th { width: 13em !important} ");
GM_addStyle(".query-builder .rules-group-container { border: 0px !important; background: rgb(233, 233, 233) !important;} .caret {  display: contents; } .bootstrap-table .fixed-table-pagination>.pagination-detail .page-list { display: inline-table; width: 200px } .fixed-table-pagination { padding-left: 20px;} .dropdown-toggle { color: black !important;}");





// Bootstrap and others
GM_addScript("https://rawgit.com/wenzhixin/bootstrap-table/master/dist/bootstrap-table.min.js");
GM_addScript("https://cdn.jsdelivr.net/npm/alasql@0.5");
GM_addScript("https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js");
GM_addScript("https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js");

GM_addScript("https://momentjs.com/downloads/moment.min.js");

// SQL Builder
GM_addScript("https://cdn.jsdelivr.net/npm/jquery-extendext@1.0.0/jquery-extendext.min.js");
GM_addScript("https://cdn.jsdelivr.net/npm/dot@1.1.3/doT.min.js");
GM_addScript("https://cdn.jsdelivr.net/npm/jQuery-QueryBuilder/dist/js/query-builder.min.js");


// Add Spinner
$("<div id = \"spinner\" style=\"width: 100%; height:100%; background-color: rgba(240, 240, 240, 0.85);position: fixed; top: 0%; left:0%\";><div class=\"spinner-border text-info\" style=\"position: fixed; top: 40%;  left: 50%; width: 3rem; height: 3rem;\" role=\"status\"></div></div>").appendTo(document.body);


$("body").append (
    '<div><img id="myNewImage" src="https://cdn.pixabay.com/photo/2016/09/15/18/29/update-1672356_960_720.png" ></img><span id="ecccTxt">by ECCC</span></div>'
);



$("#myNewImage").css ( {
    position:   "absolute",
    width:      "50px",
    top:        "23px",
    left:       "185px"
} );

$("#ecccTxt").css ( {
    position:   "absolute",
    width:      "50px",
    top:        "25px",
    left:       "245px",
    "font-size":"8px"
} );

// Overwrite Date format inside table
$("<script>function dateFormat(value, row, index) { let formattedDate =  moment(value).format('YYYY-MM-DD'); if (moment(value, \"YYYY-MM-DDTHH:mm:ss.SSSZ\", true).isValid() ) { return formattedDate} else {return value} }</script>").appendTo(document.body);



// Load All Onboarding informations into memory
var apiURL = "https://"+ addr + "/onboarding/api/v1/report/organizations?pageSize=10000";

window.addEventListener("load", function(event) {
    console.log("All resources finished loading!");

    GM_xmlhttpRequest ( {
    method:         "GET",
    url:            apiURL,
    responseType:   "json",
    onload:         processJSON_Response,
    onabort:        reportAJAX_Error,
    onerror:        reportAJAX_Error,
    ontimeout:      reportAJAX_Error
    } );
});







function processJSON_Response (rspObj) {
    if (rspObj.status != 200) {
        reportAJAX_Error (rspObj);
        return;
    }
    var myOrgs = rspObj.response;
    console.log('All organizations loaded into memory! Count: ' + myOrgs.totalCount + ' items');

    unsafeWindow.totalCount = myOrgs.totalCount;

    // Save all retrieved organizations into Global variable
    unsafeWindow.pcOrganizations = myOrgs.organizations;

    // Remove unused original elements
    $('table.table-bordered').remove(); $('.ember-view.ember-checkbox').remove(); $('.filter-checkbox-wrapper').remove();

    console.log('Build column names...');

    var sHeaders = "";
    //var sType = "";

    for (var key in myOrgs.organizations[0]) {

        switch (key) {
            case "purchaseUserId":
                break;
            case "stagedTimestamp":
                break;
            case "usedTimestamp":
                break;
            case "engagedTimestamp":
                break;
            case "purchaseTimestamp":
                break;
            case "periodEndingTimestamp":
                break;
            case "dateModified":
                break;
            case "lastLogin":
                break;
            case "trialLength":
                break;
            case "usersCountLoggedInOnce":
                break;
            case "userCommit":
                break;
            case "contractAddendumEffectiveDate":
                break;
            case "firstBillableLicenseUsageDate":
                break;
            case "rampPeriod":
                break;
            case "orgLicensedAt":
                break;
            case "lastUpdated":
                break;
            case "firstBillingPeriodStartDate":
                break;
            case "communicateUserCommit":
                break;
            case "altocloudUserCommit":
                break;
            case "userHours":
                break;
            case "createdAt":
                break;
            case "updatedAt":
                break;

            default:
                sHeaders = sHeaders + "<th data-formatter=\"dateFormat\" class=\"customCell\" data-field='" + key + "'>" + key + "</th>"
        }
        /*
        arrFilters.push({
            id: key,
            label: key,
            type: sType
        });
        */

    }

    const arrFilters = [
    {
    id: "orgName",
    label: "orgName",
    type: "string"
},
{
    id: "orgShortName",
    label: "orgShortName",
    type: "string"
},
{
    id: "orgLegalName",
    label: "orgLegalName",
    type: "string"
},
{
    id: "purchaseUserId",
    label: "purchaseUserId",
    type: "string"
},
{
    id: "purchaseUserEmail",
    label: "purchaseUserEmail",
    type: "string"
},
{
    id: "stagedTimestamp",
    label: "stagedTimestamp",
    type: "date"
},
{
    id: "signupTimestamp",
    label: "signupTimestamp",
    type: "date"
},
{
    id: "usedTimestamp",
    label: "usedTimestamp",
    type: "date"
},
{
    id: "engagedTimestamp",
    label: "engagedTimestamp",
    type: "date"
},
{
    id: "purchaseTimestamp",
    label: "purchaseTimestamp",
    type: "date"
},
{
    id: "terminationTimestamp",
    label: "terminationTimestamp",
    type: "date"
},
{
    id: "periodEndingTimestamp",
    label: "periodEndingTimestamp",
    type: "date"
},
{
    id: "dateModified",
    label: "dateModified",
    type: "date"
},
{
    id: "trialLength",
    label: "trialLength",
    type: "integer"
},
{
    id: "usersCountLoggedInOnce",
    label: "usersCountLoggedInOnce",
    type: "integer"
},
{
    id: "usersCount",
    label: "usersCount",
    type: "integer"
},
{
    id: "collaborateCount",
    label: "collaborateCount",
    type: "integer"
},
{
    id: "engageCount",
    label: "engageCount",
    type: "integer"
},
{
    id: "engageConcurrentCount",
    label: "engageConcurrentCount",
    type: "integer"
},
{
    id: "communicateCount",
    label: "communicateCount",
    type: "integer"
},
{
    id: "salesForceAddOnCount",
    label: "salesForceAddOnCount",
    type: "integer"
},
{
    id: "avtexAddOnCount",
    label: "avtexAddOnCount",
    type: "integer"
},
{
    id: "PCVMinutes",
    label: "PCVMinutes",
    type: "integer"
},
{
    id: "subscriptionType",
    label: "subscriptionType",
    type: "string"
},
{
    id: "entitlement",
    label: "entitlement",
    type: "string"
},
{
    id: "userCommit",
    label: "userCommit",
    type: "integer"
},
{
    id: "contractEffectiveDate",
    label: "contractEffectiveDate",
    type: "date"
},
{
    id: "contractAddendumEffectiveDate",
    label: "contractAddendumEffectiveDate",
    type: "date"
},
{
    id: "firstBillableLicenseUsageDate",
    label: "firstBillableLicenseUsageDate",
    type: "date"
},
{
    id: "smsMessageCount",
    label: "smsMessageCount",
    type: "integer"
},
{
    id: "inboundMessageCount",
    label: "inboundMessageCount",
    type: "integer"
},
{
    id: "rampPeriod",
    label: "rampPeriod",
    type: "string"
},
{
    id: "orgLicensedAt",
    label: "orgLicensedAt",
    type: "date"
},
{
    id: "lastUpdated",
    label: "lastUpdated",
    type: "date"
},
{
    id: "licenseModel",
    label: "licenseModel",
    type: "string"
},
{
    id: "firstBillingPeriodStartDate",
    label: "firstBillingPeriodStartDate",
    type: "date"
},
{
    id: "byocMinutes",
    label: "byocMinutes",
    type: "integer"
},
{
    id: "communicateUserCommit",
    label: "communicateUserCommit",
    type: "integer"
},
{
    id: "altocloudCount",
    label: "altocloudCount",
    type: "integer"
},
{
    id: "altocloudConcurrentCount",
    label: "altocloudConcurrentCount",
    type: "integer"
},
{
    id: "altocloudUserCommit",
    label: "altocloudUserCommit",
    type: "integer"
},
{
    id: "altocloudWebVisits",
    label: "altocloudWebVisits",
    type: "integer"
},
{
    id: "userHours",
    label: "userHours",
    type: "integer"
},
{
    id: "createdAt",
    label: "createdAt",
    type: "date"
},
{
    id: "updatedAt",
    label: "updatedAt",
    type: "date"
},
{
    id: "orgId",
    label: "orgId",
    type: "string"
},
{
    id: "trialUserEmail",
    label: "trialUserEmail",
    type: "string"
}
]


    console.log('Create table with data...');
    unsafeWindow.builderFilters = arrFilters;





    //$("<script>$('.panel-heading').remove();</script>").appendTo(document.body);
    $('.panel-heading').remove();

    // Repair Style for Bootstrap Buttons
    $("<style>.btn-danger, .btn-default, .btn-info, .btn-success, .btn-warning { background-image: none !important; color: white !important; font-size:12px !important; border-color: white !important;} </style>").appendTo(document.body);

    // Add QueryBuilder
    $("<div id='builder' data-pagination='true' class='query-builder' style='width: 900px; display: flow-root; padding-left: 20px;'><br></div><script>$('#builder').queryBuilder({ filters: " + JSON.stringify(arrFilters) + "})</script><div style='width: 1024px; display: flow-root; padding-left: 20px;'><br><button class='btn btn-secondary' type='button' id='mySubmit' >Search</button><p><h4>Results &nbsp; <span id = 'filterCount' class=\"label label-info\">" + myOrgs.totalCount + "/" + myOrgs.totalCount + "</span></h4></p><br></div>").appendTo(document.getElementsByClassName('frame-layout')[0]);

    // Custom Action For Search Button / count filter results
    $("<script>$('#mySubmit').click(() => { var sql_import_export = $('#builder').queryBuilder('getSQL').sql; sql_import_export = 'SELECT * FROM ? WHERE ' + sql_import_export;  console.log(sql_import_export); var filterCount = alasql('SELECT COUNT(1) FROM ? WHERE ' + $('#builder').queryBuilder('getSQL').sql , [pcOrganizations])[0]['COUNT(1)']; console.log('count: ' , filterCount); $('#filterCount')[0].innerHTML = filterCount + '/' + totalCount ;  $('#table').bootstrapTable('load', alasql(sql_import_export , [pcOrganizations]) ) }); </script>").appendTo(document.body);

    // Build New Table
    $("<table id='table' data-pagination='true' class='table bootstrap-table table-hover thead-dark' style='padding-left: 20px;'><thead><tr>"+ sHeaders + "</tr></thead></table>").appendTo(document.getElementsByClassName('frame-layout')[0]);

    // Feed Table with Data
    $("<script>$(function () {$('#table').bootstrapTable({data: alasql('SELECT * FROM ?' , [pcOrganizations])  }); });</script>").appendTo(document.body);

    console.log('process finished.');

    // Hide Spinner and other unused Labels
    $('#spinner')[0].hidden = true;
//    $('#spinner-background')[0].hidden = true;
    document.getElementsByClassName('pull-right')[0].hidden = true



}



function reportAJAX_Error (rspObj) {
    console.error (`TM scrpt => Error ${rspObj.status}!  ${rspObj.statusText}`);
}
