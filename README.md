# Onboarding Organizations Report
This page is altered by Tempermonkey Script (available only for Chrome browser) by ECCC Team.
It delivers more advanced options to search through PureCloud organizations.

![Onboarding Page](https://bitbucket.org/eccemea/onboarding-extended-ts/raw/master/img/onboarding.png)

## Steps to Install
* Use Chrome Browser
* Download [Tempermonkey Script](https://chrome.google.com/webstore/detail/tampermonkey/dhdgffkkebhmkfjojejmpbldmpobfkfo)
* Once Addin is installed, click on it's icon and navigate to ``Create New User Script`` and Save it with a default name.

![Create New User Script](https://bitbucket.org/eccemea/onboarding-extended-ts/raw/master/img/create%20new%20script.png)

* When Script is saved, you'll be taken to the main Tempermonkey dashboard, with list of all installed Scripts. Click on newly saved one to edit it.
* Navigate to Settings, and inside ``Updates`` section fill in ``Update URL`` with link: https://bitbucket.org/eccemea/onboarding-extended-ts/raw/master/script.js and hit ``Save``.

![New Script URL](https://bitbucket.org/eccemea/onboarding-extended-ts/raw/master/img/config%20new%20script.png)

* Exit to main Dashboard page and click on Tempermonkey extension icon then select ``Check for User Script Updates``

![Update All](https://bitbucket.org/eccemea/onboarding-extended-ts/raw/master/img/update.png)

* A warning will appear that default path for updates changed, and new version is available, hit ``Update`` button to confirm.

![Update Config Script](https://bitbucket.org/eccemea/onboarding-extended-ts/raw/master/img/Update%20main%20script.png)

* At the end you can refresh Dashboard Page to confirm that you've got installed newest version of the Script and it's enabled.

![Main Dashboard](https://bitbucket.org/eccemea/onboarding-extended-ts/raw/master/img/MainDashboard.png)


## Usage

Navigate to the [Onboarding Page](https://apps.mypurecloud.ie/onboarding/#/billingOrganizations) and authentiacate as usual. 
After a while you'll notice slightly changed UI and a ``Number 1`` indicator on Tempermonkey Addon, signaling that you're using 1 Script in background.

![active script](https://bitbucket.org/eccemea/onboarding-extended-ts/raw/master/img/activeScript.png)